﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Rocket : MonoBehaviour {

    [SerializeField] float rcsThrust = 100f;
    [SerializeField] float MainThrust = 100f;
    [SerializeField] float LevelLoadTime = 2f;
    
    [SerializeField] AudioClip MainEngineSound;
    [SerializeField] AudioClip WinSound;
    [SerializeField] AudioClip DeathSound;

    [SerializeField] ParticleSystem MainEngineParticle;
    [SerializeField] ParticleSystem WinParticle;
    [SerializeField] ParticleSystem DeathParticle;


    Rigidbody rigidBody;
    AudioSource audioSource;

    enum State { Alive, Dying, Transcending }
    State state = State.Alive;

    bool collisionsAreEnabled = true;
    int NextLevel;
    int LevelOnDeath;

    // Use this for initialization


	void Start () {
        rigidBody = GetComponent<Rigidbody>();
        audioSource = GetComponent<AudioSource>();
        if ((SceneManager.GetActiveScene().buildIndex + 1) >= SceneManager.sceneCountInBuildSettings)
        {
            NextLevel = 0;
        }
        else
            NextLevel = SceneManager.GetActiveScene().buildIndex + 1;
        LevelOnDeath = SceneManager.GetActiveScene().buildIndex;
    }

    // Update is called once per frame
    void Update() {
        if (!(state == State.Dying || state == State.Transcending))
        {
            ProcessThrust();
            ProcessRotate();
        }
        if (Debug.isDebugBuild)
        {
            RespondToDebugKeys();
        }
        
    }

    private void RespondToDebugKeys()
    {
        if (Input.GetKeyDown(KeyCode.L))
        {
            LoadNextScene();
        }
        else if (Input.GetKeyDown(KeyCode.C))
        {
            collisionsAreEnabled = !collisionsAreEnabled; //toggle
        }
    }

    private void ProcessThrust()
    {
        
        if (Input.GetKey(KeyCode.Space) || Input.GetKey(KeyCode.W))
        {
            float ThrustChange = MainThrust * Time.deltaTime;
            rigidBody.AddRelativeForce(Vector3.up * ThrustChange);
            if (!audioSource.isPlaying)
            {
                audioSource.PlayOneShot(MainEngineSound);
            }
            MainEngineParticle.Play();
        }
        else
        {
            audioSource.Stop();
            MainEngineParticle.Stop();
        }
    }

    private void ProcessRotate()
    {
        rigidBody.angularVelocity = Vector3.zero;
        
        float rotationSpeed = rcsThrust * Time.deltaTime;

        if (Input.GetKey(KeyCode.A) && Input.GetKey(KeyCode.D))
        {
            print("Can't rotate in opposite directions simultaneously!");
        }
        else if (Input.GetKey(KeyCode.A))
        {
            transform.Rotate(Vector3.forward * rotationSpeed);
        }
        else if (Input.GetKey(KeyCode.D))
        {
            transform.Rotate(-Vector3.forward * rotationSpeed);
        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collisionsAreEnabled)
        {
            switch (collision.gameObject.tag)
            {
                case "Friendly":
                    print("OK");
                    break;
                case "Finish":
                    StartSuccessSequence();
                    break;
                default:
                    StartDeathSequence();
                    break;
            }
        }
        
    }
    
    private void StartSuccessSequence()
    {
        if (state != State.Transcending)
        {
            state = State.Transcending;
            audioSource.Stop();
            audioSource.PlayOneShot(WinSound);
        }

        WinParticle.Play();
        Invoke("LoadNextScene", LevelLoadTime);
    }

    private void StartDeathSequence()
    {
        
        print("dead");
        if (state != State.Transcending)
        {
            state = State.Dying;
            audioSource.Stop();
        }
        audioSource.PlayOneShot(DeathSound);
        DeathParticle.Play();
        Invoke("LoadDeathLevel", LevelLoadTime);
    }
    private void LoadNextScene()
    {
        SceneManager.LoadScene(NextLevel);
    }

    private void LoadDeathLevel()
    {
        SceneManager.LoadScene(LevelOnDeath);
    }
}
